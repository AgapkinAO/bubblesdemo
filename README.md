**Реализация эффекта выделения с помощью шейдеров.**

Референс:

Моя демка: [демо-ролик](https://drive.google.com/file/d/0BzS974rfChQTSnB2VlFwSHhZeEE/view?usp=sharing)

Для реализации использовался stencil буфер.
Вся магия в двух шейдерах: [StencilMask.shader](https://bitbucket.org/AgapkinAO/bubblesdemo/raw/a6becff011a719f62a0aae8fab27a38513f85d8e/Assets/StencilMask.shader) и [StencilSensitive.shader](https://bitbucket.org/AgapkinAO/bubblesdemo/raw/a6becff011a719f62a0aae8fab27a38513f85d8e/Assets/StencilSensitive.shader)