﻿Shader "StencilSensitive" 
{
	Properties 
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color", COLOR) = (1, 1, 1, 1)
	}

	SubShader 
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" "ForceNoShadowCasting"="True" "IgnoreProjector"="True"}

		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha

		Stencil
		{
			Ref 1
			Comp NotEqual
		}

		CGPROGRAM

		#pragma surface surf NoLighting keepalpha noforwardadd noambient noshadow nofog

		struct Input 
		{
			fixed2 uv_MainTex;
		};

		sampler2D _MainTex;
		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = _Color.rgb;
			o.Alpha = c.a * _Color.a;
		}

		half4 LightingNoLighting(SurfaceOutput i, half3 lightDir, half atten)
		{
			fixed4 color;
			color.rgb = i.Albedo;
			color.a = i.Alpha;
			
			return color;
		}

		ENDCG
	} 

	Fallback "Diffuse"

}
