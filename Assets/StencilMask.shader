﻿Shader "StencilMask" 
{
	Properties 
	{
		_MainTex ("Mask", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType" = "AlphaTest" "Queue" = "AlphaTest" "ForceNoShadowCasting"="True" "IgnoreProjector"="True"}		

		ZWrite Off
		ZTest Always
		ColorMask 0

		Stencil
		{
			Ref 1
			Comp Always
			Pass Replace
		}

		CGPROGRAM

		#pragma surface surf NoLighting noforwardadd noambient noshadow nofog

		struct Input 
		{
			fixed2 uv_MainTex;
		};

		sampler2D _MainTex;

		void surf (Input IN, inout SurfaceOutput o)
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			
			clip(c.r - 0.3);

			o.Albedo = c.rgb;
		}

		half4 LightingNoLighting(SurfaceOutput i, half3 lightDir, half atten)
		{
			fixed4 color;
			color.rgb = i.Albedo;
			color.a = i.Alpha;
			
			return color;
		}

		ENDCG
	} 

	Fallback "Diffuse"

}
