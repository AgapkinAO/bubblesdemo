﻿using UnityEngine;
using System.Collections;

public class MovingController : MonoBehaviour 
{
    public Rigidbody rigidBody;
    public float speed = 4.0f;

    void FixedUpdate()
    {
       Vector2 force = Random.insideUnitCircle;
       float kX = Random.Range(-1 * speed, 1 * speed);
       float kZ = Random.Range(-1 * speed, 1 * speed);

       rigidBody.AddForce(force.x * kX, -0.5f, force.y * kZ, ForceMode.Acceleration);
    }
}
